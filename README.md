# mCLOUD WLANp RSU

Zu erwartende WLANp Signalstärke von Road Side Units, virtuell platziert an Unfallschwerpunkten in Deutschland

![Screenshot](Screenshot-CartoX-Frontend.png)

Dieses Repository liefert die zu erwartende Signalstärken von Car-2-X Kommunikationsinfrastruktur (5,9GHz WLANp / ETSI ITS-G5) als OpenData für die [mCLOUD](https://mcloud.de) des Bundesministerium für Verkehr und digitale Infrastruktur. Es ist im Rahmen des [mFUND Projekts CartoX<sup>2</sup>](https://www.bmvi.de/goto?id=350148) entstanden. Mit den `.geojson` Dateien ist es möglich eine Road Side Unit (RSU) an Unfallorten zu platzieren und damit abzuschätzen, ob die Unfallpartner mit Car-2-X Kommunikation ausreichend Funk-Signalstärke zum Austausch von Daten zur Verfügung gehabt hätten und damit eventuell einen Zusammenstoß hätten verhindern können.
Weiterhin ist es für Kommunen und Infrastrukturbetreiber möglich eine Abschätzung zur Funkausbreitung zu treffen, würden sie diese Orte mit Road Side Units ausstatten.

Die GeoJSON Dateien können mit Open Source Software wie [QGIS](https://www.qgis.org/de/site/) geöffnet bzw. visualisiert oder alternativ direkt online bei Anbietern wie [Carto](https://carto.com) als Datensatz eingelesen werden.

**Achtung: Dieses Repository ist 57GB groß!**

## Unfallschwerpunkte gemäß Unfallatlas

Die Unfallorte 2018 mit PKW Beteiligung mit Unfallart "Zusammenstoß mit einbiegendem/kreuzendem Fahrzeug" und Unfalltyp "Fahr-/Abbiege-/Einbiege-/Kreuzenunfall" wurden dem [Unfallatlas](https://unfallatlas.statistikportal.de) entnommen. Die Namen der `.geojson` Dateien entsprechen den IDs der Unfälle des Unfallatlas'. Die Dateien sind sortiert in Ordner nach Bundesland und Unterordner nach Gemeindeschlüssel. In den `.geojson` Dateien ist eine Feature Collection von Punkten enthalten, welche eine Mesh-Grid Funkausbreitung 500m rund um den Unfallschwerpunkt mit der entsprechenden zu erwartenden Signalstärke (RSSI in dBm) abbilden.

## Bundesländer Codes (`ULAND`)

Die 16 Bundesländer (`ULAND`) sind in Unterordner sortiert, darin widerum die Landkreise (`UKREIS`). Die Kodierung entspricht dem [Amtlichen Gemeindeschlüssel](https://www.destatis.de/DE/ZahlenFakten/LaenderRegionen/Regionales/Gemeindeverzeichnis/Administrativ/Archiv/GVAuszugQ/AuszugGV3QAktuell.html) ab. Die Kodierung der Bundesländer ist:

1. Schleswig-Holstein
2. Hamburg
3. Niedersachsen
4. Bremen
5. Nordrhein-Westfalen
6. Hessen
7. Rheinland-Pfalz
8. Baden-Württemberg
9. Bayern
10. Saarland
11. Berlin
12. Brandenburg
13. Mecklenburg-Vorpommern
14. Sachsen
15. Sachsen-Anhalt
16. Thüringen

[Siehe Karte...](https://mechlabengineering.carto.com/builder/d052d0e4-f5e4-4be0-b88b-559d391ea4ee/embed)